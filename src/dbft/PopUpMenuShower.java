/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbft;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 *
 * @author mukhsin
 */
public class PopUpMenuShower extends JPopupMenu {

    public PopUpMenuShower(Home home, String namaTabel){
        JMenuItem jMenuItemPHF = new JMenuItem("Primary Horizontal Fragmentation (PHF)");
        jMenuItemPHF.addActionListener((ActionEvent e) -> {
            PHF1 phf1 = new PHF1(home, namaTabel);
            phf1.setVisible(true);
//            home.dispose();
        });
        add(jMenuItemPHF);

        JMenu jMenuDHF = new JMenu("Derived Horizontal Fragmentation (DHF)");
        jMenuDHF.setEnabled(false);
//        jMenuItemDHF.addActionListener((ActionEvent e) -> {
//            DHF1 dhf1 = new DHF1();
//            dhf1.setVisible(true);
//            home.dispose();
//        });
        add(jMenuDHF);
        
        JMenuItem menuItemTambahData = new JMenuItem("Tambah Data");
        menuItemTambahData.addActionListener((ActionEvent e) -> {
            Tabel tabel = null;
            for (Tabel t : home.daftarTabel) {
                if (t.getNamaTabel().equals(namaTabel)) {
                    tabel = t;
                }
            }
            home.tambahData(tabel);
//            home.dispose();
        });
        add(menuItemTambahData);
        
        JMenuItem menuItemDropTabel = new JMenuItem("Drop Tabel");
        menuItemDropTabel.addActionListener((ActionEvent e) -> {
            home.dropTabel(namaTabel);
//            home.dispose();
        });
        add(menuItemDropTabel);
        
        if (home.daftarTabel.isEmpty()) {
            jMenuItemPHF.setEnabled(false);
            jMenuDHF.setEnabled(false);
            menuItemTambahData.setEnabled(false);
        }
        
        for (Tabel tabel : home.daftarTabel) {
            if (tabel.getNamaTabel().equals(namaTabel)) {
                if (tabel.getDaftarTabelParen() != null &&
                        tabel.getDaftarTabelParen().size() > 0) {
                    jMenuDHF.setEnabled(true);
                    for (Tabel.Paren tabelParen : tabel.getDaftarTabelParen()) {
                        ArrayList<Tabel> daftarTabelFragmentasi = home.getDaftarTabelFragmentasi(tabelParen);
                        if (!daftarTabelFragmentasi.isEmpty()) {
                            JMenu jMenu = new JMenu(tabelParen.toString());
                            for (int indexKolom : home.Object2IntAL(home.getIndexKolom(tabelParen, daftarTabelFragmentasi).get(0))) {
                                for (Tabel paren : home.daftarTabel) {
                                    if (paren.getNamaTabel().equals(tabelParen.getParentTableName())) {
                                        String namaKolom = paren.getDaftarKolom().get(indexKolom);
                                        JMenuItem jMenuItem = new JMenuItem(namaKolom);
                                        jMenuItem.addActionListener((ActionEvent e) -> {
                                            home.handleDHF(paren.getNamaTabel(), namaTabel, indexKolom,
                                                    home.getIndexKolom(tabelParen, daftarTabelFragmentasi));
                                        });
                                        jMenu.add(jMenuItem);
                                    }
                                }
                            }
                            jMenuDHF.add(jMenu);
                        }
                    }
//                    jMenuDHF.setEnabled(true);
//                    for (Tabel.Paren tabelParen : tabel.getDaftarTabelParen()) {
//                        ArrayList<Tabel> daftarTabelFragmentasi = home.getDaftarTabelFragmentasi(tabelParen);
//                        if (!daftarTabelFragmentasi.isEmpty()) {
//                            JMenu jMenu = new JMenu(tabelParen.toString());
//                            for (int indexKolom : home.Object2IntAL(home.getIndexKolom(tabelParen, daftarTabelFragmentasi).get(0))) {
//                                for (Tabel paren : home.daftarTabel) {
//                                    if (paren.getNamaTabel().equals(tabelParen.getParentTableName())) {
//                                        String namaKolom = paren.getDaftarKolom().get(indexKolom);
//                                        JMenuItem jMenuItem = new JMenuItem(namaKolom);
//                                        jMenuItem.addActionListener((ActionEvent e) -> {
//                                            home.handleDHF(paren.getNamaTabel(), namaTabel, indexKolom,
//                                                    home.getIndexKolom(tabelParen, daftarTabelFragmentasi));
//                                        });
//                                        jMenu.add(jMenuItem);
//                                    }
//                                }
//                            }
//                            jMenuDHF.add(jMenu);
//                        }
//                    }
                }
            }
        }
    }
}
