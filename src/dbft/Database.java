/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbft;

import java.util.ArrayList;

/**
 *
 * @author mukhsin
 */
public class Database {
    
    private String namaDatabase;
    private ArrayList<String> daftarTabel;

    public Database(String namaDatabase) {
        this.namaDatabase = namaDatabase;
    }

    public Database(String namaDatabase, ArrayList<String> daftarTabel) {
        this.namaDatabase = namaDatabase;
        this.daftarTabel = daftarTabel;
    }

    public String getNamaDatabase() {
        return namaDatabase;
    }

    public void setNamaDatabase(String namaDatabase) {
        this.namaDatabase = namaDatabase;
    }

    public ArrayList<String> getDaftarTabel() {
        return daftarTabel;
    }

    public void setDaftarTabel(ArrayList<String> daftarTabel) {
        this.daftarTabel = daftarTabel;
    }
    
}
