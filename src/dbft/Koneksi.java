/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbft;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author mukhsin
 */
public class Koneksi {
    
    public static final String HOSTNAME = "localhost";
    public static String USERNAME = "root";
    public static String PASSWORD = "";
    public static String DATABASE = "dbft";
    
    public static Connection getKoneksi() throws ClassNotFoundException, SQLException{
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://" + HOSTNAME +
                "/" +"?user=" + USERNAME +
                "&password=" + PASSWORD;
        return DriverManager.getConnection(url);
    }
}
