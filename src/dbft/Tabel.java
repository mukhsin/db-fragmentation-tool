/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbft;

import static dbft.Koneksi.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mukhsin
 */
public class Tabel {
    
    private String namaTabel;
    private final String primaryKey;
    private final ArrayList<String> daftarKolom;
    private final ArrayList<String> daftarTipeData;
    private final ArrayList<Integer> daftarSizeData;
    private final ArrayList<Boolean> daftarAutoIncrement;
    private ArrayList<ArrayList<Object>> daftarData;
    private final Class[] classes;
    
    private final ArrayList<Paren> daftarTabelParen;

    public Tabel() {
        this.namaTabel = "";
        this.primaryKey = "";
        this.daftarKolom = null;
        this.daftarTipeData = null;
        this.daftarSizeData = null;
        this.daftarAutoIncrement = null;
        this.daftarData = null;
        this.classes = null;
        
        this.daftarTabelParen = null;
    }

    public Tabel(final String namaTabel,
                 final String primaryKey,
                 final ArrayList<String> daftarKolom,
                 final ArrayList<String> daftarTipeData,
                 final ArrayList<Integer> daftarSizeData,
                 final ArrayList<Boolean> daftarAutoIncrement) {
        this.namaTabel = namaTabel;
        this.primaryKey = primaryKey;
        this.daftarKolom = daftarKolom;
        this.daftarTipeData = daftarTipeData;
        this.daftarSizeData = daftarSizeData;
        this.daftarAutoIncrement = daftarAutoIncrement;
        this.daftarData = null;
        this.classes = null;
        
        this.daftarTabelParen = null;
    }

    public Tabel(final String namaTabel,
                 final String primaryKey,
                 final ArrayList<String> daftarKolom,
                 final ArrayList<String> daftarTipeData,
                 final ArrayList<Integer> daftarSizeData,
                 final ArrayList<Boolean> daftarAutoIncrement,
                 final ArrayList<ArrayList<Object>> daftarData) {
        this.namaTabel = namaTabel;
        this.primaryKey = primaryKey;
        this.daftarKolom = daftarKolom;
        this.daftarTipeData = daftarTipeData;
        this.daftarSizeData = daftarSizeData;
        this.daftarAutoIncrement = daftarAutoIncrement;
        this.daftarData = daftarData;
        
        this.daftarTabelParen = null;
        
        classes = new Class[daftarTipeData.size()];
        initClasses(daftarTipeData);
    }

    public Tabel(final String namaTabel,
                 final String primaryKey,
                 final ArrayList<String> daftarKolom,
                 final ArrayList<String> daftarTipeData,
                 final ArrayList<Integer> daftarSizeData,
                 final ArrayList<Boolean> daftarAutoIncrement,
                 final ArrayList<ArrayList<Object>> daftarData,
                 final ArrayList<Paren> daftarTabelParen) {
        this.namaTabel = namaTabel;
        this.primaryKey = primaryKey;
        this.daftarKolom = daftarKolom;
        this.daftarTipeData = daftarTipeData;
        this.daftarSizeData = daftarSizeData;
        this.daftarAutoIncrement = daftarAutoIncrement;
        this.daftarData = daftarData;
        this.daftarTabelParen = daftarTabelParen;
        
        classes = new Class[daftarTipeData.size()];
        initClasses(daftarTipeData);
    }
    
    public void create() {
        try {
            System.out.println("Creating table:\n" + namaTabel);
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://" + HOSTNAME +
                    "/" + DATABASE +
                    "?user=" + USERNAME +
                    "&password=" + PASSWORD;
            Connection connection = DriverManager.getConnection(url);
            Statement stmt = connection.createStatement();
            
            String infoKolom = "";
            for (int i = 0; i < daftarKolom.size(); i++) {
                String namaKolom = daftarKolom.get(i);
                String tipeData = daftarTipeData.get(i);
                int sizeData = daftarSizeData.get(i);
                if (i == 0) {
                    infoKolom += namaKolom + " ";
                } else {
                    infoKolom += ", " + namaKolom + " ";
                }
                
                infoKolom += tipeData + "(" + sizeData + ")";
                
                if (daftarAutoIncrement.get(i)) {
                    infoKolom += " auto_increment";
                }
                
                if (i == daftarKolom.size() - 1) {
                    if (primaryKey != null && !primaryKey.equals("")) {
                        infoKolom += ", primary key(" + primaryKey + ")";
                    }
                }
            }
            
            String sql = "CREATE TABLE " + namaTabel + "(" + infoKolom + ");";
            System.out.println("Executing SQL:\n" + sql);
            stmt.executeUpdate(sql);
            System.out.println("Table created\n");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Tabel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Tabel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void populateData(Tabel tabel, int indeksKolom, String[] range) {
        System.out.println("Populating data into tabel: " + namaTabel);
        if (range.length == 1) {
            String range0 = range[0];
            String[] rangeBaru = new String[2];
            rangeBaru[0] = range0;
            rangeBaru[1] = range0;
            range = null;
            range = rangeBaru;
        }
        
        daftarData = new ArrayList<>();
        if (range.length > 1) {
            int awal;
            int akhir;
            if (daftarTipeData.get(indeksKolom).equals("INT")) {
                awal = Integer.valueOf(range[0]);
                akhir = Integer.valueOf(range[1]);
            
                for (int i = 0; i < tabel.getDaftarData().size(); i++) {
                    int data = Integer.valueOf(String.valueOf(tabel.getDaftarData().get(i).get(indeksKolom)));
                    if (data >= awal && data <= akhir) {
                        daftarData.add(tabel.getDaftarData().get(i));
                    }
                }
            } else {
                awal = Utils.getAlphabetNumber(range[0]);
                akhir = Utils.getAlphabetNumber(range[1]);
            
                for (int i = 0; i < tabel.getDaftarData().size(); i++) {
                    String data = String.valueOf(tabel.getDaftarData().get(i).get(indeksKolom));
                    String alias = data.substring(0, 1);
                    int d = Utils.getAlphabetNumber(alias);
                    if (d >= awal && d <= akhir) {
                        daftarData.add(tabel.getDaftarData().get(i));
                    }
                }
            }
        } else {
            int awal = Utils.getAlphabetNumber(range[0]);
            if (daftarTipeData.get(indeksKolom).equals("INT")) {
                for (int i = 0; i < tabel.getDaftarData().size(); i++) {
                    int data = Integer.valueOf(String.valueOf(tabel.getDaftarData().get(i).get(indeksKolom)));
                    if (data == awal) {
                        daftarData.add(tabel.getDaftarData().get(i));
                    }
                }
            } else {
                for (int i = 0; i < tabel.getDaftarData().size(); i++) {
                    int data = Integer.valueOf(String.valueOf(tabel.getDaftarData().get(i).get(indeksKolom)));
                    if (data == awal) {
                        daftarData.add(tabel.getDaftarData().get(i));
                    }
                }
            }
        }
        
        System.out.println("Tabel populated\n");
    }
    
    public void populateDataDerived(Tabel tabelParen, Tabel tabelChild,
            int indeksPrimaryParen, int indeksKolomParen, int indeksKolomChild, String[] range) {
        System.out.println("Populating data into tabel: " + namaTabel);
        daftarData = new ArrayList<>();
        int check = Utils.getAlphabetNumber(range[0]);
        int awal;
        int akhir;
        if (check == 0) {
            awal = Integer.valueOf(range[0]);
            akhir = Integer.valueOf(range[1]);
        } else {
            awal = Utils.getAlphabetNumber(range[0]);
            akhir = Utils.getAlphabetNumber(range[1]);
        }
        
        for (int i = 0; i < tabelChild.getDaftarData().size(); i++) {
            String dataChild = String.valueOf(tabelChild.getDaftarData().get(i).get(indeksKolomChild));
            for (int j = 0; j < tabelParen.getDaftarData().size(); j++) {
                if (String.valueOf(tabelParen.getDaftarData().get(j).get(indeksPrimaryParen)).equals(dataChild)) {
                    String dataParen = String.valueOf(tabelParen.getDaftarData().get(j).get(indeksKolomParen));
                    if (check == 0) {
                        int a = Integer.valueOf(dataParen);
                        if (a >= awal && a <= akhir) {
                            daftarData.add(tabelChild.getDaftarData().get(i));
                        }
                    } else {
                        String alias = dataParen.substring(0, 1);
                        int a = Utils.getAlphabetNumber(alias);
                        if (a >= awal && a <= akhir) {
                            daftarData.add(tabelChild.getDaftarData().get(i));
                        }
                    }
                }
            }
        }
        
        System.out.println("Tabel populated\n");
    }
    
    public void insert() {
        try {
            System.out.println("Inserting data:\n" + namaTabel);
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://" + HOSTNAME +
                    "/" + DATABASE +
                    "?user=" + USERNAME +
                    "&password=" + PASSWORD;
            Connection connection = DriverManager.getConnection(url);
            Statement stmt = connection.createStatement();
            
            for (int i = 0; i < getDaftarDataAsli().size(); i++) {
                String sql = "INSERT INTO " + namaTabel +
                        " (" + getDaftarKolomStringAsli() + ") values (" + getDaftarValueAsli(i) + ");";
                System.out.println("Executing SQL:\n" + sql);
                stmt.executeUpdate(sql);
            }
            System.out.println("Data inserted\n");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Tabel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Tabel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initClasses(final ArrayList<String> daftarTipeData1) {
        for (int i = 0; i < daftarTipeData1.size(); i++) {
            String tipeData = daftarTipeData1.get(i);
            switch (tipeData) {
                case "INT":
                    classes[i] = Integer.class;
                    break;
                case "VARCHAR":
                    classes[i] = String.class;
                    break;
                default:
                    classes[i] = String.class;
                    break;
            }
        }
    }

    public String getNamaTabel() {
        return namaTabel;
    }
    
    public void setNamaTabel(String namaTabel) {
        this.namaTabel = namaTabel;
    }

    public String getPrimaryKey() {
        return primaryKey;
    }

    public ArrayList<String> getDaftarKolom() {
        return daftarKolom;
    }
    
    public String getDaftarKolomString() {
        ArrayList<String> daftarKolomTersedia = new ArrayList<>();
        for (int i = 0; i < daftarKolom.size(); i++) {
            if (!daftarAutoIncrement.get(i)) {
                daftarKolomTersedia.add(daftarKolom.get(i));
            }
        }
        
        String  kolomString = "";
        int i = 0;
        for (String namaKolom : daftarKolomTersedia) {
            if (i == 0) {
                kolomString += namaKolom;
            } else {
                kolomString += "," + namaKolom;
            }
            i++;
        }
        return kolomString;
    }
    
    public String getDaftarKolomStringAsli() {
        String  kolomString = "";
        int i = 0;
        for (String namaKolom : daftarKolom) {
            if (i == 0) {
                kolomString += namaKolom;
            } else {
                kolomString += "," + namaKolom;
            }
            i++;
        }
        return kolomString;
    }
    
    public String getDaftarValueKosong() {
        ArrayList<String> daftarKolomTersedia = new ArrayList<>();
        for (int i = 0; i < daftarKolom.size(); i++) {
            if (!daftarAutoIncrement.get(i)) {
                daftarKolomTersedia.add(daftarKolom.get(i));
            }
        }
        
        String  valueKosong = "";
        for (int i = 0; i < daftarKolomTersedia.size(); i++) {
            if (i == 0) {
                valueKosong += "\'\'";
            } else {
                valueKosong += "," + "\'\'";
            }
        }
        return valueKosong;
    }
    
    public String getDaftarValueKosongAsli() {
        String  valueKosong = "";
        for (int i = 0; i < daftarKolom.size(); i++) {
            if (i == 0) {
                valueKosong += "\'\'";
            } else {
                valueKosong += "," + "\'\'";
            }
        }
        return valueKosong;
    }
    
    public String getDaftarValueAsli(int index) {
        String  value = "";
        for (int i = 0; i < daftarKolom.size(); i++) {
            if (i == 0) {
                value += "\'" + getDaftarDataAsli().get(index).get(i) + "\'";
            } else {
                value += "," + "\'" + getDaftarDataAsli().get(index).get(i) + "\'";
            }
        }
        return value;
    }

    public ArrayList<String> getDaftarTipeData() {
        return daftarTipeData;
    }

    public ArrayList<Integer> getDaftarSizeData() {
        return daftarSizeData;
    }

    public ArrayList<Boolean> getDaftarAutoIncrement() {
        return daftarAutoIncrement;
    }

    public Class[] getClasses() {
        return classes;
    }
    
    public ArrayList<ArrayList<Object>> getDaftarDataAsli() {
        return daftarData;
    }

    public ArrayList<ArrayList<Object>> getDaftarData() {
        ArrayList<ArrayList<Object>> rows = new ArrayList<>();
        // Loop rows
        for (int i = 0; i < daftarData.get(0).size(); i++) {
            // Loop cols
            ArrayList<Object> cols = new ArrayList<>();
            for (int j = 0; j < daftarData.size(); j++) {
                cols.add(daftarData.get(j).get(i));
            }
            rows.add(cols);
        }
        return rows;
    }

    public ArrayList<Paren> getDaftarTabelParen() {
        return daftarTabelParen;
    }

    public void setDaftarData(ArrayList<ArrayList<Object>> daftarData) {
        this.daftarData = daftarData;
    }
    
    public static class Paren {
        
        private String childTableName;
        private String childForeignKey;
        private String parentTableName;
        private String parentPrimaryKey;

        public Paren(
                String childTableName,
                String childForeignKey,
                String parentTableName,
                String parentPrimaryKey) {
            this.childTableName = childTableName;
            this.childForeignKey = childForeignKey;
            this.parentTableName = parentTableName;
            this.parentPrimaryKey = parentPrimaryKey;
        }

        @Override
        public String toString() {
//            return childTableName + "." + childForeignKey + " -> " +
//                    parentTableName + "." + parentPrimaryKey;
            return parentTableName;
        }

        public String getChildTableName() {
            return childTableName;
        }

        public void setChildTableName(String childTableName) {
            this.childTableName = childTableName;
        }

        public String getChildForeignKey() {
            return childForeignKey;
        }

        public void setChildForeignKey(String childPrimaryKey) {
            this.childForeignKey = childPrimaryKey;
        }

        public String getParentTableName() {
            return parentTableName;
        }

        public void setParentTableName(String parentTableName) {
            this.parentTableName = parentTableName;
        }

        public String getParentPrimaryKey() {
            return parentPrimaryKey;
        }

        public void setParentPrimaryKey(String parentPrimaryKey) {
            this.parentPrimaryKey = parentPrimaryKey;
        }
    }
    
    public static class Fragmentasi {
        
        private String namaTabel;
        private int indexKolom;
        private String[] range;

        public Fragmentasi(String namaTabel, int indexKolom, String[] range) {
            this.namaTabel = namaTabel;
            this.indexKolom = indexKolom;
            this.range = range;
        }

        public String getNamaTabel() {
            return namaTabel;
        }

        public void setNamaTabel(String namaTabel) {
            this.namaTabel = namaTabel;
        }

        public int getIndexKolom() {
            return indexKolom;
        }

        public void setIndexKolom(int indexKolom) {
            this.indexKolom = indexKolom;
        }

        public String[] getRange() {
            return range;
        }

        public void setRange(String[] range) {
            this.range = range;
        }
    }
}
