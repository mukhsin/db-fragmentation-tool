/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbft;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JList;
import javax.swing.SwingUtilities;

/**
 *
 * @author mukhsin
 */
public class PopUpClickListener extends MouseAdapter {
    
    Home home;

    public PopUpClickListener(Home home) {
        this.home = home;
    }
    
    @Override
    public void mousePressed(MouseEvent e){
        if (e.isPopupTrigger())
            doPop(e);
    }

    @Override
    public void mouseReleased(MouseEvent e){
        if (e.isPopupTrigger())
            doPop(e);
    }

    private void doPop(MouseEvent e){
        String namaTabel = "";
        if (SwingUtilities.isRightMouseButton(e)) {
            JList list = (JList)e.getSource();
            int row = list.locationToIndex(e.getPoint());
            list.setSelectedIndex(row);

            namaTabel = (String) list.getSelectedValue();
        }

        PopUpMenuShower menu = new PopUpMenuShower(home, namaTabel);
        menu.show(e.getComponent(), e.getX(), e.getY());
    }
}
